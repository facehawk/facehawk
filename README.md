<a href="https://facehawk.co.uk/"><img src="Face_Hawk.png" /></a>
<br /><br /><br />
<h1 align="center">Face Recognition System Back-End Services </h1>

<h2><b> Features: </b></h2>
<b>
<ol>
<li> RealTime Face Recognition </li>
<li> RealTime Events/Alerts Update </li>
<li> RealTime Auto/Manual Enrollment/Search/Update/Remove Face </li>
<li> Age Prediction </li>
<li> Gender Prediction </li>
<li> Mood Prediction (Happy, Sad, Fear, Upset, Angry, Disgust) </li>
<li> Mask Detection </li>
<li> Race Prediction </li>
<li> Report Generation </li>
<li> Report Visualization/Analytics </li>
</ol>
</b>

<h2>System Requirements</h2>
<ul>
<li>OS: Ubuntu-16 or above</li>
<li>NVIDIA Driver: 410 or above</li>
<li>CUDA Version: 10.0 (Tested) or above</li>
<li>CUDNN Version: 7.6.5 (Tested) or above</li>
<li>Pthon Version: 3.7 (Tested) or above</li>
<li>GPU: 8GB for 4 cameras (Recommended)</li>
<li>CPU: Core i7 (3.1 GHz)</li>
</ul>


<h2>Future Modules </h2>

These backend services/features are under development.

<ol>
<li>Crowd Analysis </li>
<li>Individual Detection and Tracking </li>
<li>Facial Recognition based Attendance System </li>
<li>Facial Recognition based Border Management </li>
<li>3D Facial Key Points and Landmarks Detection </li>
<li>2D to 3D Morphing </li>
<li>3D Face Recognition </li>
<li>Human Action/Activity Recognition </li>
<li>Weapon Detection </li>
<li>Violence (Fight etc.) Detection </li>
</ol>


```bash
Author: Monis Ali
Date: 10-03-2021
```
